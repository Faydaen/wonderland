<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Client
 *
 * @property integer $id
 * @property integer $chat_id
 * @property string $comment
 * @property integer $project_id
 * @property string $username
 * @property string $first_name
 * @property string $last_name
 * @property string $parameters
 * @property string $tmp_parameters
 * @property string $chat_history
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @method static \Illuminate\Database\Query\Builder|\App\Client whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Client whereChatId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Client whereComment($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Client whereProjectId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Client whereUsername($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Client whereFirstName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Client whereLastName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Client whereParameters($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Client whereTmpParameters($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Client whereChatHistory($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Client whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Client whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Client whereDeletedAt($value)
 * @mixin \Eloquent
 * @property string $process_history
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Query\Builder|\App\Client whereProcessHistory($value)
 */
class Client extends Model
{
    protected $guarded = ['id'];



    public function getParametersAttribute(){
        return json_decode($this->attributes['parameters'], false);
    }

    public function getTmpParametersAttribute(){
        return json_decode($this->attributes['tmp_parameters'], false);
    }

    public function getProcessHistoryAttribute(){
        return json_decode($this->attributes['process_history'], false);
    }


    public function user(){
        return $this->hasOne('App\User');
    }

//take


//    public function setParametersAttribute($parameters){
//
//        if (empty($parameters)){
//
//        }


//        if (empty($json)){
//            $this->attributes['json'] = '{}';
//        }
//        else
//        {
//            $this->attributes['json'] = json_encode($json);
//        }
//    }






}
