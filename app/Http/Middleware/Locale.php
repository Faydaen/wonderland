<?php

namespace App\Http\Middleware;

use Auth;
use Closure;
use Config;
use Session;

class Locale
{


    public function handle($request, Closure $next)
    {

        if (!Session::has('locale')) {
            $configLanguage = Config::get('app.locale');
            if (Auth::check()){
                $userLanguage = Auth::user()->profile->language;
                $lang = $userLanguage ?: $configLanguage;
            }
            else{
                $lang = $configLanguage;
            }

            Session::put('locale', $lang);
        }

        app()->setLocale(Session::get('locale'));

        return $next($request);
    }
}
