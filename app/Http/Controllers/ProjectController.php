<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use App\Project;


class ProjectController extends Controller
{



    public function __construct()
    {
        $this->middleware('auth');
    }

    #region просмотр
    /**
     * Список
     */
    public function showList()
    {
        $projects = Project::list();
        return view('project.list', compact('projects'));
    }

    /**
     * Конкретный
     */
    public function show($id)
    {
        $project = Project::findOrFail($id);
//        return $project;
        return view('tree.project', compact('project'));
    }

    public function tree($id){
        return Project::tree($id);
//
//        $a = [
//            'text'=>'Проект',
//            'type'=>'project',
//            'state'=>['opened'=>true],
//            'a_attr'=> ['href'=>'http://www.google.com'],
//            'children'=>[
//                [
//                    'text'=>'Процесс 1',
//                    'type'=>'process',
//                    'state'=>['opened'=>true],
//                    'children'=>[
//                        [
//                            'text'=>'Интент 1',
//                            'type'=>'intent'
//                        ],
//                        [
//                            'text'=>'Интент 2',
//                            'type'=>'intent'
//                        ],
//                    ]
//                ],
//                [
//                    'text'=>'Процесс 2',
//                    'type'=>'process'
//                ],
//            ]
//
//        ];
//
//        return response()->json($a);
    }


    #endregion

    #region создать
    /**
     * Создать (форма)
     */
    public function showCreateForm()
    {
        return view('project.create');
    }

    /**
     * Создать
     */
    public function store(Request $request)
    {

        $projectInput = \Request::only(['apiai_developer_token', 'apiai_client_token', 'telegram_token', 'file']);
        $entityInput = \Request::only(['title', 'description']);

        $entity = Entity::create($entityInput);


        $entity->tags()->attach($request->input('tags'));

        $project = new Project();

        if (\Request::hasFile('file')) {
            $clientImage = \Request::file('file');
            $project->saveLogo($clientImage);
        }

        $project->apiai_developer_token = $projectInput['apiai_developer_token'];
        $project->apiai_client_token = $projectInput['apiai_client_token'];
        $project->telegram_token = $projectInput['telegram_token'];
        $project->user_id = Auth::user()->id;
        $project->entity_id = $entity->id;
        $project->save();

        return $this->redirect($project->id);
    }
    #endregion

    #region редактировать
    /**
     * Редактировать (форма)
     */
    public function showUpdateForm($projectId)
    {
        $project = Project::findOrFail($projectId);
        return view('project.edit', compact('project'));
    }

    /**
     * Редактировать
     */
    public function update(Request $request, $id)
    {
        $project = Project::findOrFail($id);

        if (\Request::hasFile('file')) {
            $clientImage = \Request::file('file');
            $project->saveLogo($clientImage);
        }

        if (\Request::has('destroy_image')) {
            $project->destroyLogo();
        }

        $project->apiai_developer_token = \Request::get('apiai_developer_token');
        $project->apiai_client_token = \Request::get('apiai_client_token');
        $project->telegram_token = \Request::get('telegram_token');
        $project->user_id = Auth::user()->id;
        $project->save();

        $entity = $project->entity;
        $entity->title = \Request::get('title');
        $entity->description = \Request::get('description');

        $entity->tags()->sync($request->input('tags',[]));

        $entity->save();

        return $this->redirect($project->id);
    }
    #endregion


    private function redirect($projectId){

        if (\Request::has('save')) {
            return redirect()->route('projects.show', ['id' => $projectId]);
        };

        if (\Request::has('apply')) {
            return redirect()->route('projects.edit',['id'=>$projectId]);
        };
    }

    #region удалить

    /**
     * Удалить навсегда
     */
    public function destroy($id)
    {
        $project = Project::onlyTrashed()->where('id', $id)->first();
        $project->destroyLogo();

        // незабыть про удаление отношений
        $project->forceDelete();
        return back();
    }


    /**
     * Востановить
     */
    public function restore($id)
    {
        Project::onlyTrashed()->where('id', $id)->restore();
        return back();
    }


    /**
     * Удаленные (просмотреть что в корзине)
     */
    public function showTrash()
    {
        $projects = Project::trash();
        return view('project.trashList', compact('projects'));
    }



    /**
     * Удалить
     */
    public function delete($id)
    {
        $project = Project::findOrFail($id);
        $project->delete();
        return back();
    }
    #endregion



}
