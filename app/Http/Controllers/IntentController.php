<?php

namespace App\Http\Controllers;

use App\Intent;
use Illuminate\Http\Request;

use App\Http\Requests;

class IntentController extends Controller
{


    public function show($id){
        $intent = Intent::with('process.project')->findOrFail($id);
        return view('tree.intent',compact('intent'),['action'=>'edit']);
    }

    public function showList(){

    }

    public function showCreateForm(){
        return view('tree.intent.create');
    }

    public function showUpdateForm(){

    }
    public function store(){

    }
    public function update($intentId){
        $intent = Intent::findOrFail($intentId);

        $intent->userSaysText = \Request::get('says');
        $intent->outputContextText = \Request::get('output_context');


        $intent->title = \Request::get('title');
        $intent->use_apiai = \Request::has('use_apiai');
        $intent->reset_contexts = \Request::has('reset_contexts');
        $intent->fallback = \Request::has('fallback');
        $intent->code = \Request::get('code');
        $intent->script = \Request::get('script');
        $intent->description = \Request::get('description');
        $intent->save();
        return back();
    }
    public function destroy(){

    }
    public function moveToTrash(){

    }
    public function restoreFormTrash(){

    }
    public function showTrash(){

    }

    public function sync($id){
        $intent = Intent::findOrFail($id);
        $intent->export();

    }
}
