<?php

namespace App\Http\Controllers;


use Auth;
//use Illuminate\Http\Request;
//use App\Http\Requests;
use Session;


class UserController extends Controller
{
    public function __construct()
    {
//        $this->middleware('auth');
    }

    public function setLanguage($lang){
        $validator = \Validator::make(
            ['language'=>$lang],
            ['language' => 'required|in:ru,en,fr,kl']);

        if ($validator->passes()){
            if (Auth::check()){
                Auth::user()->profile->language = $lang;
                Auth::user()->profile->save();
            }
            Session::put('locale',$lang);
        }

        return back()->withErrors($validator);
    }


    public function showMyProfile(){
        dd(Auth::user());
    }
}
