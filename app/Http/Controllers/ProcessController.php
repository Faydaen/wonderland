<?php

namespace App\Http\Controllers;

use App\Process;
use Illuminate\Http\Request;

use App\Http\Requests;

class ProcessController extends Controller
{

    

    public function show($processId){
        $process = Process::findOrFail($processId);
        return view('tree.process',compact('process'));
    }

    public function showCreateForm(){
//        return view('tree.process');
    }
    
    public function showUpdateForm($processId){
        $process = Process::with('intents')->findOrFail($processId);
        return view('tree.process',compact('process'));

    }
    public function store(){

    }
    public function update(){

    }
    public function destroy(){

    }
    public function moveToTrash(){

    }
    public function restoreFormTrash(){

    }
    public function showTrash(){

    }

    public function tree($processId){
        $process = Process::with('intents')->findOrFail($processId);
        return $process->tree();
    }

    public function sync($id){

    }
}
