<?php

namespace App;

//use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Auth;


/**
 * App\Project
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $title
 * @property string $description
 * @property string $logo
 * @property boolean $telegram_enable
 * @property string $telegram_token
 * @property string $telegram_name
 * @property boolean $whatsapp_enable
 * @property string $whatsapp_token
 * @property string $whatsapp_name
 * @property boolean $viber_enable
 * @property string $viber_token
 * @property string $viber_name
 * @property boolean $facebook_enable
 * @property string $facebook_token
 * @property string $facebook_name
 * @property boolean $dev_mode
 * @property string $apiai_client_token
 * @property string $apiai_developer_token
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Query\Builder|\App\Project whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Project whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Project whereTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Project whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Project whereLogo($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Project whereTelegramEnable($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Project whereTelegramToken($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Project whereTelegramName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Project whereWhatsappEnable($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Project whereWhatsappToken($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Project whereWhatsappName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Project whereViberEnable($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Project whereViberToken($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Project whereViberName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Project whereFacebookEnable($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Project whereFacebookToken($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Project whereFacebookName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Project whereDevMode($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Project whereApiaiClientToken($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Project whereApiaiDeveloperToken($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Project whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Project whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Project whereDeletedAt($value)
 * @mixin \Eloquent
 * @property string $telegram
 * @property string $whatsapp
 * @property string $viber
 * @property string $facebook
 * @property string $apiai
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Process[] $process
 * @method static \Illuminate\Database\Query\Builder|\App\Project whereTelegram($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Project whereWhatsapp($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Project whereViber($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Project whereFacebook($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Project whereApiai($value)
 */
class Project extends Model
{
    protected $guarded = ['id'];

    use SoftDeletes;

    #region аксесеры
    public function getTelegramAttribute(){
        return json_decode($this->attributes['telegram'], false);
    }

    public function getWhatsappAttribute(){
        return json_decode($this->attributes['whatsapp'], false);
    }

    public function getViberAttribute(){
        return json_decode($this->attributes['viber'], false);
    }

    public function getFacebookAttribute(){
        return json_decode($this->attributes['facebook'], false);
    }

    public function getApiaiAttribute(){
        return json_decode($this->attributes['apiai'], false);
    }

    public function getStateAttribute(){
        return json_decode($this->attributes['state'], false);
    }
    #endregion

    #region мутаторы
    public function setStateAttribute($value){
        $this->attributes['state'] = json_encode($value);
    }
    #endregion
    
    
    
    

    static public function list(){
        return Project::where('user_id',Auth::id())->paginate();
    }

    static public function trash(){
        return Project::onlyTrashed()->where('user_id',Auth::id())->paginate();
    }


    static public function tree($id){
        $project = Project::with('process.intents')->whereId($id)->first();
        $projectUrl = route('project.show',['projectId'=> $project->id]);
        $tree = [
            'text'=>$project->title,
            'type'=>'project',
            'state'=>$project->state,
            'a_attr'=> ['href'=>$projectUrl],
            'children'=>$project->children()
        ];

        return $tree;

    }

    public function children(){
        $children = [];
        foreach($this->process as $process){
            $children[] = $process->showNode();
        }
        return $children;
    }








    #region отношения
    public function process(){
        return $this->hasMany('App\Process');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }
    #endregion

    #region импорт
    /**
     * импортирует все интенты из апи аи
     * новые создает, старые обновляет
     *
     * @return array
     */
    public function importIntentsFromApiAi(){

        $apiAiIntents = $this->getAllIntentsFromApiAi();

        $countUpdated = 0;
        $countCreated = 0;
        foreach($apiAiIntents as $apiAiIntent){
            $intent = Intent::where('iid',$apiAiIntent->id)->first();
            if($intent){ // обновить
                $intent->import();
                $countUpdated++;
            }
            else{ // создать
                $intent = new Intent();
                $intent->iid  = $apiAiIntent->id;
                $intent->title  = $apiAiIntent->name;
                $intent->import();
                $countCreated++;
            }
        }

        return [
          'status'=>'ok',
          'created'=>$countCreated,
          'updated'=>$countUpdated,
        ];
    }



    /**
     * Получить массив всех интентов из api.ai
     *
     * @return array
     */
    private function getAllIntentsFromApiAi(){
        $url = 'https://api.api.ai/v1/intents/';
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HTTPHEADER,
            [
                'Content-Type: application/json; charset=utf-8',
                'Authorization: Bearer '.$this->apiai->devToken,
            ]
        );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);

        return json_decode($response);

    }
    #endregion





}
