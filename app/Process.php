<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Process
 *
 * @mixin \Eloquent
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property integer $project_id
 * @property integer $pos
 * @property string $code
 * @property boolean $is_system
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Intent[] $intents
 * @property-read \App\Project $project
 * @method static \Illuminate\Database\Query\Builder|\App\Process whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Process whereTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Process whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Process whereProjectId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Process wherePos($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Process whereCode($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Process whereIsSystem($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Process whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Process whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Process whereDeletedAt($value)
 */
class Process extends Model
{
    protected $guarded = ['id'];


    public function getStateAttribute(){
        return json_decode($this->attributes['state'], false);
    }

    public function setStateAttribute($value){
        $this->attributes['state'] = json_encode($value);
    }


    public function tree(){
        $tree = $this->showNode();
        $tree['children'] = $this->childNodes();
        return $tree;
    }

    public function showNode(){
        $processUrl = route('process.show',['projectId'=> $this->id]);

        return [
            'text'=>$this->title,
            'type'=>'process',
            'state'=>$this->state,
            'a_attr'=> ['href'=>$processUrl],
        ];
    }


    public function childNodes($parent_intent=null) {

        $intents = $this->intents;

        $children = [];
        foreach ($intents as $intent){
            if ($intent->parent_intent_id == $parent_intent){
                $intentUrl = route('intent.show',[$intent->id]);
                $child = [
                    'text'=>$intent->title,
                    'a_attr'=> ['href'=>$intentUrl],
                    'type'=>'intent',
                    'state'=>$intent->state,
                    'parent_intent_id' => $intent->parent_intent_id
                ];

                if ($intent->intents->count()) {
                    $child['children'] = $this->childNodes($intent->id);
                }


                $children[] = $child;
            }
        }

        return $children;


    }



    public function intents(){
        return $this->hasMany('App\Intent');
    }

    public function project(){
        return $this->belongsTo('App\Project');
    }

    public static function getOrCreateSystemProcess(){

        if (!\Session::get('currentProject')){
            throw new \Exception("Не выбран проект");
        }


        $systemProcess = self::whereIsSystem(true)->firstOrCreate([
            'title'=>'Импортированные интенты',
            'description'=>'Сюда попадают интенты из API.AI',
            'project_id'=> \Session::get('currentProject')->id,
            'is_system'=> true,
            'code'=>'System'
        ]);

        return $systemProcess;
    }

}
