<?php

namespace App;

use App\Process;
use Illuminate\Database\Eloquent\Model;
use Mockery\CountValidator\Exception;

/**
 * App\Intent
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property integer $process_id
 * @property integer $pos
 * @property string $iid
 * @property string $code
 * @property string $user_says
 * @property string $context
 * @property boolean $use_apiai
 * @property string $script
 * @property boolean $synchronized
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @method static \Illuminate\Database\Query\Builder|\App\Intent whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Intent whereTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Intent whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Intent whereProcessId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Intent wherePos($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Intent whereIid($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Intent whereCode($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Intent whereUserSays($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Intent whereContext($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Intent whereUseApiai($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Intent whereScript($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Intent whereSynchronized($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Intent whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Intent whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Intent whereDeletedAt($value)
 * @mixin \Eloquent
 * @property string $templates
 * @property string $affected_contexts
 * @property boolean $reset_contexts
 * @property string $speech
 * @property string $action
 * @property string $priority
 * @property boolean $fallback
 * @property string $sync_errors
 * @property-read \App\User $process
 * @method static \Illuminate\Database\Query\Builder|\App\Intent whereTemplates($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Intent whereAffectedContexts($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Intent whereResetContexts($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Intent whereSpeech($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Intent whereAction($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Intent wherePriority($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Intent whereFallback($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Intent whereSyncErrors($value)
 */
class Intent extends Model
{
    protected $guarded = ['id'];

    protected $casts = [
        'fallback' => 'boolean',
        'reset_contexts' => 'boolean',
        'use_apiai' => 'boolean',
        'synchronized' => 'boolean',
    ];



    public function getUserSaysTextAttribute(){
        $userSays = '';
        foreach ($this->templates as $phrase){
            $userSays .= $phrase.PHP_EOL;
        }
        return $userSays;
    }

    public function getOutputContextTextAttribute()
    {
        $outPutContext = [];
        foreach ($this->affected_contexts as $context){
            $outPutContext[] = $context->name.'='.$context->lifespan;
        }
        return implode($outPutContext,'; ');
    }

    public function getInputContextTextAttribute(){
        return implode($this->context,'; ');
    }

    public function getResponseTextAttribute(){
        return implode($this->speech,PHP_EOL);
    }



    public function setUserSaysTextAttribute($value){
        $templates = self::clearText($value,PHP_EOL);
        $this->templates = $templates;
        $user_says = [];

        foreach ($templates as $phrase){
            $user_says[] = [
              'data'=>[
                  'text'=>$phrase
              ],
              'isTemplate'=>true
            ];
        }
        $this->user_says = $user_says;
    }
    public function setOutputContextTextAttribute($value){
        $outputContexts = self::clearText($value,';');
        $affected_contexts = [];
        foreach ($outputContexts  as $outputContext){
            $match = preg_match('/\s*([^=]+)\s*=\s*(\d+)\s*/',$outputContext,$m);
            if ($match){
                $affected_contexts[] = [
                    'name'=>$m[1],
                    'lifespan'=>$m[2]
                ];
            }
        }
        $this->affected_contexts = $affected_contexts;
    }


    private static function clearText($value,$delimiter){
        return array_filter(
            array_map(function($e){
                return trim($e);
            }, explode($delimiter,$value)),
            function($element) {
                return !empty($element);
            });
    }





    #region аксесеры
    public function getContextAttribute(){
        return json_decode($this->attributes['context'], false);
    }

    public function getTemplatesAttribute(){
        return json_decode($this->attributes['templates'], false);
    }

    public function getUserSaysAttribute(){
        return json_decode($this->attributes['user_says'], false);
    }

    public function getAffectedContextsAttribute(){
        return json_decode($this->attributes['affected_contexts'], false);
    }

    public function getSpeechAttribute(){
        return json_decode($this->attributes['speech'], false);
    }

    public function getSyncErrorsAttribute(){
        return json_decode($this->attributes['sync_errors'], false);
    }

    public function getStateAttribute(){
        return json_decode($this->attributes['state'], false);
    }
    #endregion

    #region мутаторы
    public function setContextAttribute($value){
        $this->attributes['context'] = json_encode($value);
    }

    public function setTemplatesAttribute($value){
        $this->attributes['templates'] = json_encode($value);
    }

    public function setUserSaysAttribute($value){
        $this->attributes['user_says'] = json_encode($value);
    }

    public function setAffectedContextsAttribute($value){
        $this->attributes['affected_contexts'] = json_encode($value);
    }

    public function setSpeechAttribute($value){
        $this->attributes['speech'] = json_encode($value);
    }

    public function setSyncErrorsAttribute($value){
        $this->attributes['sync_errors'] = json_encode($value);
    }

    public function setStateAttribute($value){
        $this->attributes['state'] = json_encode($value);
    }
    #endregion

    #region отношения
    public function process(){
        return $this->belongsTo('App\Process');
    }
    public function parent(){
        return $this->belongsTo('App\Intent','parent_intent_id');
    }
    public function intent(){
        return $this->parent();
    }
    public function children(){
        return $this->hasMany('App\Intent','parent_intent_id');
    }
    public function intents(){
        return $this->children();
    }
    #endregion




    // todo сделать в аксесоре и мутатрое user_says чистку

    private function importWithoutSaving(){
        if (!$this->iid){
            throw new Exception("Не задан iid интента");
        }

        $intentFromApiAi = $this->getIntentFromApiAi();

        $nonSyncProcess = Process::getOrCreateSystemProcess();
        $this->process_id = $nonSyncProcess->id;
        $this->pos = 0;

        $this->templates = $intentFromApiAi['templates'];
        $this->user_says = $intentFromApiAi['user_says'] ?? [];
        $this->context = $intentFromApiAi['context'] ?? [];

        $this->affected_contexts = $intentFromApiAi['responses'][0]['affected_contexts'] ?? [];
        $this->reset_contexts = $intentFromApiAi['responses'][0]['reset_contexts'] ?? false;
        $this->speech = $intentFromApiAi['responses'][0]['speech'] ?? [];
        $this->action = $intentFromApiAi['responses'][0]['action'] ?? null;
        $this->priority = $intentFromApiAi['priority'] ?? 500000;
        $this->fallback = $intentFromApiAi['fallbackIntent'] ?? false;
        $this->synchronized = true;

    }

    public function import(){
        $this->importWithoutSaving();
        if ($this->save()){
            $this->synchronized = true;
        }
    }

    public function export(){
        if (!\Session::get('currentProject')){
            throw new Exception("Не выбран проект");
        }

        if ($this->iid){
            return $this->updateInApiAi();
        }
        else{
            return $this->createInApiAi();
        }

    }


    /**
     * Обновить интент в API.Ai
     *
     * @return array
     */
    private function updateInApiAi(){
        $url = 'https://api.api.ai/v1/intents/'.$this->iid;
        $ch = curl_init($url);
        $json = json_encode($this->makeExportArray());
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_HTTPHEADER,
            [
                'Content-Type: application/json; charset=utf-8',
                'Authorization: Bearer ' . \Session::get('currentProject')->apiai->devToken,
            ]
        );

        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);
        $result = json_decode($response,true);

        if($result['status']['code'] == 200){
            $result['id'] = $this->iid;
            $this->synchronized = true;
        }
        else{
            $this->sync_errors = $result['status']['errorType'];
        }
        $this->save();
        return $result;
    }

    /**
     * Создать интент в  API.Ai
     *
     * @return array
     */
    private function createInApiAi(){
        $url = 'https://api.api.ai/v1/intents/';
        $ch = curl_init($url);
        $json = json_encode($this->makeExportArray());
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_HTTPHEADER,
            [
                'Content-Type: application/json; charset=utf-8',
                'Authorization: Bearer ' . \Session::get('currentProject')->apiai->devToken,
            ]
        );

        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);
        $result = json_decode($response,true);

        if($result['status']['code'] == 200){
            $this->iid = $result['id'];
            $this->synchronized = true;
        }
        else{
            $this->sync_errors = $result['status']['errorType'];
        }
        $this->save();
        return $result;



    }


    /**
     * Интент в API.AI-шном формате
     *
     * @return array
     */
    private function makeExportArray(){
        return [
            "name"=>$this->process->code.$this->code.' - '.$this->title,
            "contexts"=>$this->context ?? [],
            "templates"=>$this->templates ?? [],
            "userSays"=>$this->user_says ?? [],
            "responses"=> [
                [
                    "resetContexts"=>$this->reset_contexts ?? false,
                    "action"=>$this->action ?? '',
                    "affectedContexts"=> $this->affected_contexts ?? [],
                    "speech"=>$this->speech ?? [],
                ]
            ],
            "priority"=>$this->priority ?? 500000,
            "fallbackIntent"=>$this->fallback ?? false,
        ];
    }

    /**
     * Получить интент из API.AI
     *
     * @return array
     */
    private function getIntentFromApiAi(){

        if (!\Session::get('currentProject')){
            throw new Exception("Не выбран проект");
        }

        $url = 'https://api.api.ai/v1/intents/'.$this->iid;
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HTTPHEADER,
            [
                'Content-Type: application/json; charset=utf-8',
                'Authorization: Bearer ' . \Session::get('currentProject')->apiai->devToken,
            ]
        );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);
        return json_decode($response,true);
    }


}
