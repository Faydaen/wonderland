<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class Purge extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'purge';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'purge database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        `mysql -uroot -e "drop database wonderland; create database wonderland;"`;
        $this->info('Purge done');
    }
}
