<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Profile
 *
 * @property integer $id
 * @property string $avatar
 * @property string $language
 * @property string $sex
 * @property string $phone
 * @property string $background
 * @property string $first_name
 * @property string $last_name
 * @property string $birthday
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Query\Builder|\App\Profile whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Profile whereAvatar($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Profile whereLanguage($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Profile whereSex($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Profile wherePhone($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Profile whereBackground($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Profile whereFirstName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Profile whereLastName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Profile whereBirthday($value)
 * @mixin \Eloquent
 * @property string $template
 * @method static \Illuminate\Database\Query\Builder|\App\Profile whereTemplate($value)
 */
class Profile extends Model
{
    protected $guarded = ['id'];

    public $timestamps = false;


    public function user(){
        return $this->hasOne('App\User');
    }
}
