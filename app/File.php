<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\File
 *
 * @property integer $id
 * @property string $file_type
 * @property string $file_name
 * @property string $thumbnail
 * @property boolean $uploaded_to_telegram
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @method static \Illuminate\Database\Query\Builder|\App\File whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\File whereFileType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\File whereFileName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\File whereThumbnail($value)
 * @method static \Illuminate\Database\Query\Builder|\App\File whereUploadedToTelegram($value)
 * @method static \Illuminate\Database\Query\Builder|\App\File whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\File whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\File whereDeletedAt($value)
 * @mixin \Eloquent
 * @property string $owner
 * @property integer $client_id
 * @property string $telegram_url
 * @method static \Illuminate\Database\Query\Builder|\App\File whereOwner($value)
 * @method static \Illuminate\Database\Query\Builder|\App\File whereClientId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\File whereTelegramUrl($value)
 */
class File extends Model
{
    protected $guarded = ['id'];
}
