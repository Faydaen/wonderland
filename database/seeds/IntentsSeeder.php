<?php

use App\Intent;
use Illuminate\Database\Seeder;

class IntentsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Intent::create([
            'code' => '01', // action это код бизнес процесса + этот код
            'title' => 'Приветствие', // имя интента
            'description' => 'Пользователь поздаровался',
            'process_id' => 1,
            'pos' => 1,

            'iid' => null,
            'templates'=> [
                'Здравствуйте',
                'Привет, *'
            ],
            'user_says' => [
                [
                    'isTemplate'=>false,
                    'data'=> [
                        [
                            'text'=>'Здравствуйте'
                        ]
                    ],
                ],
                [
                    'isTemplate'=>true,
                    'data'=> [
                        [
                            'text'=>'Привет, *'
                        ]
                    ],
                ],
            ],
            'context' => [
                'inputContext'
            ],
            'affected_contexts' => [
                ['name'=>'outContext1','lifespan'=>2],
                ['name'=>'outContext2','lifespan'=>2],
            ],
            'reset_contexts'=>false,
            'speech' => [
                'рад тебя видеть',
                'давно не видились'
            ],

            'state'=> ['opened'=>true],
            'action' => 'P01',
            'priority' => 500000,

            'fallback' => false,
            'use_apiai' => true,
            'script' => '@text("Привет, как тебя зовут?")',
            'sync_errors'=> [], //export_collision import_collision
            'synchronized' => false,
        ]);



        Intent::create([
            'code' => '02', // action это код бизнес процесса + этот код
            'title' => 'Прощание', // имя интента
            'description' => 'Пользователь попрощался',
            'process_id' => 1,
            'pos' => 1,

            'iid' => null,
            'templates'=> [
                'Досвидания',
                'Пока, *'
            ],
            'user_says' => [
                [
                    'isTemplate'=>false,
                    'data'=> [
                        [
                            'text'=>'Досвидания'
                        ]
                    ],
                ],
                [
                    'isTemplate'=>true,
                    'data'=> [
                        [
                            'text'=>'Пока, *'
                        ]
                    ],
                ],
            ],
            'context' => [
                'inputContext'
            ],
            'affected_contexts' => [
                ['name'=>'outContext1','lifespan'=>2],
                ['name'=>'outContext2','lifespan'=>2],
            ],
            'reset_contexts'=>false,
            'speech' => [
                'до новых встреч',
                'увидимся'
            ],

            'state'=> ['opened'=>true],
            'action' => 'P02',
            'priority' => 500000,

            'fallback' => false,
            'use_apiai' => true,
            'script' => '@text("До встречи?")',
            'sync_errors'=> [], //export_collision import_collision
            'synchronized' => false,
        ]);



        Intent::create([
            'code' => '03', // action это код бизнес процесса + этот код
            'title' => 'После приветсвея', // имя интента
            'description' => 'Пользователь сказал что то после приветсвя',
            'process_id' => 1,
            'pos' => 1,

            'iid' => null,
            'templates'=> [
                'Досвидания',
                'Пока, *'
            ],
            'user_says' => [
                [
                    'isTemplate'=>true,
                    'data'=> [
                        [
                            'text'=>'*'
                        ]
                    ],
                ],
            ],
            'context' => [
                'inputContext'
            ],
            'affected_contexts' => [
                ['name'=>'outContext1','lifespan'=>2],
                ['name'=>'outContext2','lifespan'=>2],
            ],
            'reset_contexts'=>false,
            'speech' => [
                '*',
            ],
            'parent_intent_id'=>1,
            'state'=> ['opened'=>true],
            'action' => 'P03',
            'priority' => 500000,

            'fallback' => false,
            'use_apiai' => true,
            'script' => '@text("я слышу тебя")',
            'sync_errors'=> [], //export_collision import_collision
            'synchronized' => false,
        ]);










    }
}
