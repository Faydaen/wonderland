<?php

use App\Profile;
use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ProfilesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $profile = new Profile();
        $profile->avatar =  'rAyybY4iPbniIHUugtcIuQ5TUkS4Qwpt.jpg';
        $profile->first_name = 'Jon';
        $profile->last_name = 'Wolf';
        $profile->sex = 'male';
        $profile->language = 'ru';
        $profile->phone = '+1(123)581-32-13';
        $profile->background = 'uuiOG0nuXSQrFsEZQWh7rgz8r71lSP33.jpg';
        $profile->template = 'spacelab';
        $profile->birthday = Carbon::create('1988','2','1','1','0','0');
        $profile->save();
    }
}
