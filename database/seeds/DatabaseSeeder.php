<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersSeeder::class);
        $this->call(ProfilesSeeder::class);
        $this->call(ClientsSeeder::class);
        $this->call(ProjectsSeeder::class);
        $this->call(ProcessesSeeder::class);
        $this->call(IntentsSeeder::class);
        $this->call(FilesSeeder::class);
    }
}
