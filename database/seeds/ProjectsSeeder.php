<?php
use App\Project;
use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ProjectsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $project = new Project();
        $project->user_id = 1;
        $project->title = 'TalkBank';
        $project->description = 'TalkBank - первый в мире виртуальный банк';
        $project->logo = 'YF532PzPIwuxirjSRe1rurZZ43EcjCFD.png';

        $project->telegram = json_encode([
            'enable'=>true,
            'token'=>env('TELEGRAM_TOKEN'),
            'name'=>'TalkBank'
        ]);

        $project->whatsapp = json_encode(['enable'=>false]);
        $project->viber = json_encode(['enable'=>false]);
        $project->facebook = json_encode(['enable'=>false]);

        $project->apiai = json_encode([
            'clientToken'=>env('APIAI_CLIENT_TOKEN'),
            'devToken'=>env('APIAI_DEV_TOKEN'),
        ]);

        $project->state = ['opened'=>true];

        
        $project->dev_mode = true;
        $project->apiai_developer_token = $project->save();







    }
}
