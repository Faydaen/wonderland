<?php
use App\Process;

use Illuminate\Database\Seeder;

class ProcessesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Process::create([
            'title'=>'Опрос',
            'description'=>'Опрос, какую рассылку слать',
            'project_id'=>1,
            'pos'=>1,
            'state'=> ['opened'=>true],
            'code'=>'P'
        ]);


    }
}
