<?php
use App\User;
use Illuminate\Database\Seeder;
use Carbon\Carbon;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->name =  'Ghost';
        $user->email =  'ghost@wonderland';
        $user->password =  Hash::make('ghost@wonderland');
        $user->client_id = 1;
        $user->profile_id =  1;
        $user->save();
    }
}
