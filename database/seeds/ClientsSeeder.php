<?php

use App\Client;
use Illuminate\Database\Seeder;

class ClientsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Client::create([
            'chat_id'=>'197248556',
            'comment'=>'',
            'project_id'=>'1',
            'username'=>'Faydaen',
            'first_name'=>'Евгений',
            'last_name'=>'Панов',
            'parameters'=>  json_encode([
                'haveCard'=>true,
                'subscribeDay'=>[
                    'Mon',
                    'Fri'
                ]
            ]),
            'tmp_parameters'=>json_encode([]),
            'process_history'=>json_encode([])
        ]);
    }
}


