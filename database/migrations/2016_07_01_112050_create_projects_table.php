<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');

            $table->string('title')->nullable();
            $table->text('description')->nullable();

            $table->string('logo')->nullable();

            $table->json('telegram')->nullable();
            $table->json('whatsapp')->nullable();
            $table->json('viber')->nullable();
            $table->json('facebook')->nullable();
            $table->json('state')->nullable();

            $table->boolean('dev_mode')->default(true);
            $table->string('apiai')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('projects');
    }
}
