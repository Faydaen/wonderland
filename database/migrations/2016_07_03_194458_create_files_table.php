<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('files', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('file_type',[
                'image',
                'animation',
                'sticker',
                'document',
                'audio',
                'voice',
                'visio'
            ]);
            $table->string('file_name');
            $table->enum('owner',['server','client'])->default('server');
            $table->integer('client_id')->nullable()->default(null);
            $table->string('thumbnail')->nullable();
            $table->string('telegram_url')->nullable();
            $table->boolean('uploaded_to_telegram')->default(false);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
