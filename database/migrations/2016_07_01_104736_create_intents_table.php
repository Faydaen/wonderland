<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIntentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('intents', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code')->nullable(); //код внутри бизнес процесса
            $table->string('title')->nullable();
            $table->string('description')->nullable();
            $table->integer('process_id');

            $table->integer('parent_intent_id')->nullable();;

            $table->integer('pos')->default(0);

            $table->string('iid')->nullable();
            $table->json('templates')->nullable();
            $table->json('user_says')->nullable();
            $table->json('context')->nullable();
            $table->json('affected_contexts')->nullable();
            $table->boolean('reset_contexts')->default(false);
            $table->json('speech')->nullable(); // если один то записать в нулевой элемент массива
            $table->string('action')->nullable();
            $table->string('priority')->nullable();
            $table->boolean('fallback')->default(false);



            $table->json('state')->nullable();


            $table->boolean('use_apiai')->default(true);
            $table->text('script')->nullable();
            $table->boolean('synchronized')->default(false);
            $table->json('sync_errors')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
