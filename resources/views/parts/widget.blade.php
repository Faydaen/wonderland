<div class="panel panel-default">
    <div class="panel-heading">{{ $name }}</div>

    <div class="panel-body text-center">

        <i class="fa {{ $icon }} fa-5x"></i>
        <br><br>
        <div class="btn btn-block btn-info">@lang('menu.look')</div>

    </div>
</div>