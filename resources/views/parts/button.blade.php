{{--

получаемые параметры

method
route
id
icon

дополнительно (если method==patch)
name
value


todo сделать предачу класса и текста

--}}

@if($method == 'patch')
    {{ Form::open(['method'=>'patch', 'route'=>[$route,'id'=>$id,'name'=>$name],'style'=>'display: inline-block']) }}
    @else
    {{ Form::open(['method'=>$method, 'route'=>[$route,'id'=>$id],'style'=>'display: inline-block']) }}
@endif
    <button class="btn"><i class="fa {{ $icon }}"></i></button>
    @if($method == 'patch')
        {{Form::hidden($name,$value)}}
    @endif
{{ Form::close() }}