@extends('layout')

@section('title') {{ trans('project.create') }} @endsection




@section('header')

@endsection




@section('content')
    <div class="well">
        @include('tree.tree',['treeUrl'=>route('process.tree', ['id'=>$process->id] )])
        <hr>
        @include('tree.process.ui')
        <hr>
        @include('tree.process.edit')
    </div>
@endsection
