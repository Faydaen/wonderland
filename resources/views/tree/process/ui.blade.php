<a href="{{route('process.create')}}" class="btn btn-info">
    <i class="fa fa-plus"></i>
    @lang('process.create')
</a>

<a href="{{route('process.sync',$process->id)}}" class="btn btn-info">
    <i class="fa fa-refresh"></i>
    @lang('process.sync')
</a>
