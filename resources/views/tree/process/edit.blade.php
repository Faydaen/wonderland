
{{Form::open(['route'=>['process.edit',$process->id],'method'=>'post'])}}

<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            {!! Form::label('title', trans('common.title'), ['class' => 'control-label']) !!}
            <div class="input-group margin-bottom-sm">
                <span class="input-group-addon"><i class="fa fa-terminal"></i></span>
                {!! Form::text('title', null, ['class' => 'form-control', 'placeholder'=>trans('common.title.placeholder')]) !!}
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('description',trans('common.description')) !!}
            {!! Form::textarea('description',null,['size' => '30x6', 'class'=>'form-control','placeholder'=>trans('common.description.placeholder')]) !!}
        </div>
    </div>
</div>

{!! Form::close() !!}

