<div class="btn-group pull-right">
    <button name="save" value="yes" type="submit" class="btn btn-primary"><i class="fa fa-check"></i>
        @lang('common.save')
    </button>
</div>
<a href="{{route('process.list',['processId'=>$intent->process_id])}}" type="submit" class="btn btn-default"><i class="fa fa-arrow-left"></i>
    @lang('common.back')
</a>