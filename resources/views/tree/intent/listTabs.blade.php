<div>
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation">
            <a href="#project" aria-controls="project" role="tab" data-toggle="tab">
                @lang('project.project')
            </a>
        </li>
        <li role="presentation"  class="active">
            <a href="#process" aria-controls="process" role="tab" data-toggle="tab">
                @lang('process.process')
            </a>
        </li>
        <li role="presentation">
            <a href="#flowchart" aria-controls="flowchart" role="tab" data-toggle="tab">
                @lang('intent.flowchart')
            </a>
        </li>
        <li role="presentation">
            <a href="#trash" aria-controls="trash" role="tab" data-toggle="tab">
                @lang('intent.trash')
            </a>
        </li>
    </ul>
    <br>
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane" id="project">
            @include('tree.list.projectTree',['treeUrl'=>route('project.tree',['id'=>$intent->process->project_id])])
        </div>
        <div role="tabpanel" class="tab-pane  active" id="process">
            @include('tree.list.processTree',['treeUrl'=>route('process.tree',['id'=>$intent->process_id])])
        </div>
        <div role="tabpanel" class="tab-pane" id="flowchart">
            @include('tree.intent.list.flowchart')
        </div>
        <div role="tabpanel" class="tab-pane" id="trash">
            @include('tree.intent.list.trash')
        </div>
    </div>
</div>
