
<script>
    $(function () {
        var editor = ace.edit("editor");
        editor.session.setMode({path: "ace/mode/php", inline: true});


        editor.$blockScrolling = Infinity;
        //editor.focus();


        editor.getSession().on('change', function (e) {
            $('#intentScript').val(editor.getValue());
        });

    });
</script>




<pre id="editor">{{ $intent->script }}</pre>

<div style="display: none">
    {{-- не оставлять пробелов между <textarea>(текст)</textarea>--}}
    <textarea name="script" placeholder="Скрипт" class="form-control" rows="9" id="intentScript">{{ $intent->script }}</textarea>
    <span class="help-block">Используйте дерево выше, чтобы генерировать текст срипта</span>
</div>


