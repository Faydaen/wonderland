<script>
    $(document).ready(function () {
        var use_apiai = document.querySelector('#use_apiai');
        new Switchery(use_apiai);
    });
</script>



<div class="row">


    <div class="col-md-3">
        <div class="form-group">
            {!! Form::label('code', trans('common.code'), ['class' => 'control-label']) !!}
            <div class="input-group margin-bottom-sm">
                <span class="input-group-addon"><i class="fa fa-hashtag"></i></span>
                {!! Form::text('code', $intent->code, ['class' => 'form-control', 'placeholder'=>trans('common.code.placeholder')]) !!}
            </div>
        </div>
    </div>

    <div class="col-md-9">
        <div class="form-group">
            {!! Form::label('title', trans('common.title'), ['class' => 'control-label']) !!}
            <div class="input-group margin-bottom-sm">
                <span class="input-group-addon"><i class="fa fa-terminal"></i></span>
                {!! Form::text('title', $intent->title, ['class' => 'form-control', 'placeholder'=>trans('common.title.placeholder')]) !!}
            </div>
        </div>
    </div>

    <div class="col-md-12">

        <div class="form-group">
            {!! Form::label('description',trans('common.description')) !!}
            {!! Form::textarea('description',$intent->description,['size' => '30x6', 'class'=>'form-control','placeholder'=>trans('common.description.placeholder')]) !!}
        </div>
    </div>







</div>

<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            {!! Form::checkbox('use_apiai',null,$intent->use_apiai,['class'=>'js-switch','id'=>'use_apiai']) !!}
            {!! Form::label('use_apiai',trans('intent.use_apiai')) !!}
        </div>
    </div>
</div>

<hr>







