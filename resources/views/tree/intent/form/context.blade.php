<script>
    $(document).ready(function () {
        var reset_contexts = document.querySelector('#reset_contexts');
        new Switchery(reset_contexts);
    });
</script>

<div class="row">


    {{--входящий--}}

    <div class="col-md-12">
        <div class="form-group">
            {!! Form::label('context_in', trans('intent.input_context'), ['class' => 'control-label']) !!}
            <div class="input-group margin-bottom-sm">
                <span class="input-group-addon"><i class="fa fa-arrow-right"></i></span>
                {!! Form::text('context_in', $intent->inputContextText, ['class' => 'form-control', 'placeholder'=>trans('intent.input_context.placeholder')]) !!}
            </div>
        </div>
        <span class="help-block">@lang('intent.input_context.helper')</span>
    </div>


</div>


<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            {!! Form::label('output_context', trans('intent.output_context'), ['class' => 'control-label']) !!}
            <div class="input-group margin-bottom-sm">
                <span class="input-group-addon"><i class="fa fa-arrow-left"></i></span>
                {!! Form::text('output_context', $intent->outputContextText, ['class' => 'form-control', 'placeholder'=>trans('intent.output_context.placeholder')]) !!}
            </div>
        </div>
        <span class="help-block">@lang('intent.output_context.helper')</span>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            {!! Form::checkbox('reset_contexts',null,$intent->reset_contexts,['class'=>'js-switch','id'=>'reset_contexts']) !!}
            {!! Form::label('reset_contexts',trans('intent.reset_context')) !!}
        </div>
    </div>
</div>








