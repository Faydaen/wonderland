<script>
    $(document).ready(function () {
        var fallback = document.querySelector('#fallback');
        new Switchery(fallback);
    });
</script>


<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            {!! Form::label('response',trans('intent.response')) !!}
            {!! Form::textarea('response',$intent->responseText,['size' => '30x6', 'class'=>'form-control','placeholder'=>trans('intent.response.placeholder')]) !!}
            <span class="help-block">@lang('intent.response.helper')</span>
        </div>
    </div>


    <div class="col-md-12">
        {!! Form::label('action', trans('intent.action'), ['class' => 'control-label']) !!}
        <div class="input-group margin-bottom-sm">
            <span class="input-group-addon"><i class="fa fa-cogs"></i></span>
            {!! Form::text('action', $intent->action, ['class' => 'form-control', 'placeholder'=>trans('intent.action.placeholder')]) !!}
        </div>
    </div>


</div>

<br>
<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            {!! Form::checkbox('fallback',null,$intent->fallback,['class'=>'js-switch','id'=>'fallback']) !!}
            {!! Form::label('fallback',trans('intent.fallback')) !!}
        </div>
    </div>
</div>
