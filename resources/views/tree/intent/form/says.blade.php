

<div class="row">
    <div class="col-md-12">

        {!! Form::label('priority', trans('intent.priority'), ['class' => 'control-label']) !!}
        <div class="input-group margin-bottom-sm">
            <span class="input-group-addon"><i class="fa fa-list-ol"></i></span>
            {!! Form::number('priority', $intent->priority, ['class' => 'form-control', 'placeholder'=>trans('intent.priority.placeholder')]) !!}
        </div>

    </div>
</div>

<br>

<div class="row">


    <div class="col-md-12">

        <div class="form-group">
            {!! Form::label('says',trans('intent.says')) !!}
            {!! Form::textarea('says',$intent->userSaysText,['size' => '30x6', 'class'=>'form-control','placeholder'=>trans('intent.says.placeholder')]) !!}
            <span class="help-block">@lang('intent.says.helper')</span>

        </div>

    </div>
</div>



