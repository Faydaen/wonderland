<div>
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation">
            <a href="#form" aria-controls="form" role="tab" data-toggle="tab">
                @if($action == 'edit')
                    @lang('intent.edit')
                @else
                    @lang('intent.intent')
                @endif
            </a>
        </li>
        <li role="presentation" class="active">
            <a href="#script" aria-controls="script" role="tab" data-toggle="tab">
                @lang('intent.script')
            </a>
        </li>
        <li role="presentation">
            <a href="#says" aria-controls="says" role="tab" data-toggle="tab">
                @lang('intent.says')
            </a>
        </li>
        <li role="presentation">
            <a href="#context" aria-controls="context" role="tab" data-toggle="tab">
                @lang('intent.context')
            </a>
        </li>
        <li role="presentation">
            <a href="#response" aria-controls="response" role="tab" data-toggle="tab">
                @lang('intent.response')
            </a>
        </li>
    </ul>
    <br>

    <div class="tab-content">
        <div role="tabpanel" class="tab-pane" id="form">
            @include('tree.intent.form.edit')
        </div>
        <div role="tabpanel" class="tab-pane active" id="script">
            @include('tree.intent.form.scriptAccordion')
        </div>
        <div role="tabpanel" class="tab-pane" id="says">
            @include('tree.intent.form.says')
        </div>
        <div role="tabpanel" class="tab-pane" id="context">
            @include('tree.intent.form.context')
        </div>
        <div role="tabpanel" class="tab-pane" id="response">
            @include('tree.intent.form.response')
        </div>

    </div>

</div>
