<a href="{{route('intent.create')}}" class="btn btn-info btn-sm">
    <i class="fa fa-plus"></i>
    @lang('intent.create')
</a>

<a href="{{route('intent.sync',$intent->id)}}" class="btn btn-info btn-sm">
    <i class="fa fa-refresh"></i>
    @lang('intent.sync')
</a>


<a href="{{route('intent.delete',$intent->id)}}" class="btn btn-danger btn-sm pull-right">
    <i class="fa fa-trash"></i>
    @lang('intent.delete')
</a>

{{--<a href="{{route('intent.delete',$intent->id)}}" class="btn btn-info">--}}
    {{--<i class="fa fa-refresh"></i>--}}
    {{--@lang('intent.delete)--}}
{{--</a>--}}


