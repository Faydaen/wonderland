

<script>
    $(function () {

        $("#processTree").jstree({
            "core": {
                "data": {
                    "url": "{{$treeUrl}}",
                    "data": function (node) {
                        return {'id': node.id};
                    }
                },
                "check_callback": true,
                "multiple": false // нельзя выдлеить несколько узлов одновременно
            },
            "types": {
                "project": {
                    "icon": "fa fa-folder-open",
                    "valid_children": ["process"] // нельзя добавить дочерний узел
                },
                "process": {
                    "icon": "fa fa-cubes",
                    "valid_children": ["intent"]
                },
                "intent": {
                    "icon": "fa fa-cube",
                    "valid_children": ["intent"]
                },
                "system_process":{
                    "icon": "fa fa-refresh",
                    "valid_children": ["intent"]
                }
            },
            "plugins": ["types", "search"]
        }).on('select_node.jstree', function (e, data) {
            var href = data.node.a_attr.href;
            document.location.href = href;
        });





    });
</script>


<div id="processTree"></div>

