@extends('layout')

@section('title') {{ trans('project.create') }} @endsection




@section('header')

@endsection




@section('content')
    <div class="well">

        @include('tree.intent.listTabs')

        <hr>
        @include('tree.intent.ui')
        <hr>
        {{Form::open(['route'=>['intent.update',$intent->id],'method'=>'put'])}}

        @include('tree.intent.formTabs')

        @include('tree.intent.buttons')

        {!! Form::close() !!}


    </div>

@endsection
