@extends('layout')

@section('title') {{ trans('project.create') }} @endsection




@section('header')

@endsection




@section('content')
    <div class="well">
        @include('tree.tree',['treeUrl'=>route('project.tree',['id'=>$project->id])])
    </div>
@endsection
