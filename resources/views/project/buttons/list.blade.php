@include('parts.button',[
    'method'=>'get',
    'route'=>'project.show',
    'id'=>$project->id,
    'icon'=>'fa-eye',
    'name'=>'enable',
    'value'=>'0',
])

@include('parts.button',['method'=>'get','route'=>'project.edit','id'=>$project->id,'icon'=>'fa-pencil'])

@include('parts.button',['method'=>'delete','route'=>'project.destroy','id'=>$project->id,'icon'=>'fa-trash'])
