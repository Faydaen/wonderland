@extends('layout')

@section('title')
    @lang('project.create')
@endsection




@section('header')

@endsection




@section('content')
    <div class="well">
        <h2>@lang('project.create')</h2>
        {{Form::open(['route'=>'project.create','method'=>'post','files'=>true])}}
        @include('project.form',['action'=>'create'])
        {!! Form::close() !!}
    </div>
@endsection
