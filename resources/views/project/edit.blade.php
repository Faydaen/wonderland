@extends('layout')

@section('title')
    @lang('project.edit')
@endsection




@section('header')

@endsection




@section('content')
    <div class="well">
        <h2>@lang('project.edit')</h2>
        {{Form::model($project,['route'=>['project.update','id'=>$project->id],'method'=>'put', 'files'=>true])}}
        @include('project.form',['action'=>'save'])
        {!! Form::close() !!}
    </div>
@endsection
