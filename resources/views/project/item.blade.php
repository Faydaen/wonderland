<div class="panel panel-default">

    {{--    <div class="panel-heading"><h4>{{ $project->title  }}</h4></div>--}}


    <div class="panel-body">


        <div class="row">
            <div class="col-md-2">
                @if($project->logo)
                    <img width="150" src="/uploads/logos/{{$project->logo}}" alt="{{$project->title}}"
                         class="img-circle">
                @else
                    <img width="150" src="/images/img.png" alt="{{$project->title}}" class="img-circle">
                @endif
            </div>
            <div class="col-md-3">
                {{--<h3>{{ $project->title  }}</h3>--}}
                <h4>{{ $project->title }}</h4>
                <p>{{ $project->description }}</p>


            </div>

            <div class="col-md-7 text-right">
                <br>
                <br>
                <br>


                {{--@if($trash)--}}
                    {{--@include('project.buttons.trash')--}}
                {{--@else--}}
                    @include('project.buttons.list')
                {{--@endif--}}


            </div>
        </div>


        {{--<a href="{{route('project.show',['id'=>$project->id])}}" class="btn btn-primary" role="button">{{trans('common.show')}}</a>--}}
        {{--<a href="/project/{{$project->id}}/edit" class="btn btn-primary" role="button">{{trans('common.edit')}}</a>--}}
        {{--@if($trash == true)--}}
        {{--<a href="#" class="btn btn-default" role="button">{{ trans('common.delete')  }}</a>--}}
        {{--<a href="#" class="btn btn-default" role="button">{{ trans('common.restore') }}</a>--}}
        {{--@else--}}
        {{--<a href="#" class="btn btn-default" role="button">{{ trans('common.delete') }}</a>--}}
        {{--@endif--}}

    </div>
</div>

