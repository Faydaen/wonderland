@extends('layout')

@section('title') {{ trans('project.list') }} @endsection




@section('header')

@endsection




@section('content')
    <div class="well">
        <h2>@lang('project.list.trash')</h2>
        <hr>

        @include('project.menu')

        <hr>


        @forelse($projects as $project)
            @include('project.trashItem')
        @empty
            @lang('project.list.empty.trash')
        @endforelse


        @if($projects->total() > $projects->perPage())
            <div class="well well-sm text-center">
                <div> {{ $projects->links() }} </div>
            </div>
        @endif


    </div>
@endsection
