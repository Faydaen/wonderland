


<div class="row">


    @if ($action == 'save')
        <div class="col-md-3">
            @if ($project->logo)
                <img src="/uploads/logos/{{$project->logo}}" width="250" class="img-thumbnail">
            @else
                <img src="/images/img.png" width="250" class="img-thumbnail">
            @endif
        </div>
    @else
        <div class="col-md-3">
            <img src="/images/img.png" width="250" class="img-thumbnail">
        </div>
    @endif

    <div class="col-md-9">
        <div class="form-group">
            {!! Form::label('title', trans('common.title'), ['class' => 'control-label']) !!}
            <div class="input-group margin-bottom-sm">
                <span class="input-group-addon"><i class="fa fa-terminal"></i></span>
                {!! Form::text('title', null, ['class' => 'form-control', 'placeholder'=>trans('common.title.placeholder')]) !!}
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('description',trans('common.description')) !!}
            {!! Form::textarea('description',null,['size' => '30x6', 'class'=>'form-control','placeholder'=>trans('common.description.placeholder')]) !!}
        </div>
    </div>

</div>


<div class="form-group">
    {!! Form::label('file',trans('project.logo')) !!}<br>
    {{ Form::file('file',['title'=> '<i class="fa fa-picture-o"></i> ' . trans('common.image.browse'),'class'=>'btn-info']) }}
    <span class="help-block">
        @lang('common.image.browse.help',['size'=>'250x250'])
    </span>
</div>



@if ($action == 'save')
    @if ($project->logo)
        <div class="form-group">
            {!! Form::checkbox('destroy_image',null,null,['class'=>'js-switch-danger','id'=>'destroy_image']) !!}
            {!! Form::label('destroy_image',trans('common.image.destroy')) !!}
        </div>
    @endif
@endif




<div class="form-group">
    {!! Form::label('apiai_client_token', trans('project.apiai_client_token'), ['class' => 'control-label']) !!}
    <div class="input-group margin-bottom-sm">
        <span class="input-group-addon"><i class="fa fa-terminal"></i></span>
        {!! Form::text('apiai_client_token', null, ['class' => 'form-control', 'placeholder'=>trans('project.apiai_client_token.placeholder')]) !!}
    </div>
    <span class="help-block">
        @lang('project.apiai_client_token.helper')
    </span>
</div>

<div class="form-group">
    {!! Form::label('apiai_developer_token', trans('project.apiai_developer_token'), ['class' => 'control-label']) !!}
    <div class="input-group margin-bottom-sm">
        <span class="input-group-addon"><i class="fa fa-terminal"></i></span>
        {!! Form::text('apiai_developer_token', null, ['class' => 'form-control', 'placeholder'=>trans('project.apiai_developer_token.placeholder')]) !!}
    </div>
    <span class="help-block">
        @lang('project.apiai_developer_token.helper')
    </span>
</div>


<div class="form-group">
    {!! Form::label('telegram_token', trans('project.telegram_token'), ['class' => 'control-label']) !!}
    <div class="input-group margin-bottom-sm">
        <span class="input-group-addon"><i class="fa fa-terminal"></i></span>
        {!! Form::text('telegram_token', null, ['class' => 'form-control', 'placeholder'=>trans('project.telegram_token.placeholder')]) !!}
    </div>
    <span class="help-block">@lang('project.telegram_token.helper')</span>
</div>




<hr>




<hr>

<div class="btn-group pull-right">
    <button name="apply" value="yes" type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i>
        @lang('common.apply')
    </button>
    <button name="save" value="yes" type="submit" class="btn btn-primary"><i class="fa fa-check"></i>
        @lang('common.'.$action)
    </button>
</div>
<a href="{{route('project.list')}}" type="submit" class="btn btn-default"><i class="fa fa-arrow-left"></i>
    @lang('common.back')
</a>




