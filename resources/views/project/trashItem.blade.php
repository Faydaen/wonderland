<div class="panel panel-default">

    {{--    <div class="panel-heading"><h4>{{ $project->title  }}</h4></div>--}}


    <div class="panel-body">


        <div class="row">
            <div class="col-md-2">
                @if($project->logo)
                    <img width="150" src="/uploads/logos/{{$project->logo}}" alt="{{$project->title}}"
                         class="img-circle">
                @else
                    <img width="150" src="/images/img.png" alt="{{$project->title}}" class="img-circle">
                @endif
            </div>
            <div class="col-md-3">
                {{--<h3>{{ $project->title  }}</h3>--}}
                <h4>{{ $project->title }}</h4>
                <p>{{ $project->description }}</p>


            </div>

            <div class="col-md-7 text-right">
                <br>
                <br>
                <br>


                @include('project.buttons.trash')


            </div>
        </div>




    </div>
</div>

