<div class="btn-group">
    <a href="{{route('project.create')}}" class="btn btn-default">
        <i class="fa fa-plus"></i>
        @lang('project.create')
    </a>

    <a href="{{route('project.list')}}" class="btn btn-default">
        <i class="fa fa-list"></i>
        @lang('project.list')
    </a>

    <a href="{{route('project.trash')}}" class="btn btn-default">
        <i class="fa fa-trash"></i>
        @lang('project.list.trash')
    </a>
</div>




