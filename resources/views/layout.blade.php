<!doctype html>
<html lang="{{ Session::get('locale') }}">
<head>
    <meta charset="UTF-8">
    <title>@yield('title')</title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <link rel="icon" type="image/ico" href="/favicon.ico" />





    <link rel="stylesheet" href="{{ elixir("compiled/main.css") }}">
    <script type="text/javascript" src="{{ elixir("compiled/main.js") }}"></script>

    <script src="/jstree/dist/jstree.min.js"></script>
    <link rel="stylesheet" href="/jstree/dist/themes/default/style.min.css"/>

    <script src="/ace-builds/src/ace.js"></script>
    <script src="/ace-builds/src/ext-settings_menu.js"></script>
    <style type="text/css" media="screen">
        #editor {
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            height: 300px;
        }
    </style>
    @yield('header')

</head>
<body>
<div class="container">
    @include('menu')
    @yield('content')
</div>
</body>
</html>