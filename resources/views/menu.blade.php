<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <a class="navbar-brand" href="{{ url('/') }}">
                {{ config('app.name', 'Laravel') }}
            </a>

        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">


            @if(Auth::check())

            <form class="navbar-form navbar-left" role="search">
                <div class="form-group">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="@lang('menu.search') ">

                        <span class="input-group-btn">
                        <button type="submit" class="btn btn-info">
                            <i class="fa fa-search"></i>
                        </button>
                        </span>
                    </div>
                </div>
            </form>
            @endif




            <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right">
                @if (Auth::guest())
                    <li><a href="{{ url('/login') }}">@lang('auth.login')</a></li>
                    <li><a href="{{ url('/register') }}">@lang('auth.register')</a></li>
                @else

                    {{-- Меню --}}
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                           aria-expanded="false">@lang('menu.menu') <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="/projects">@lang('menu.projects') </a></li>
                            <li class="divider"></li>
                            <li><a href="/projects">@lang('menu.users') </a></li>
                            <li class="divider"></li>
                            <li><a href="/projects">@lang('menu.payments') </a></li>
                            <li class="divider"></li>
                            <li><a href="/projects">@lang('menu.statistics') </a></li>
                            <li class="divider"></li>
                            <li><a href="/projects">@lang('menu.files') </a></li>
                            <li class="divider"></li>
                            <li><a href="/projects">@lang('menu.modules') </a></li>
                            <li class="divider"></li>
                            <li><a href="/projects">@lang('menu.help') </a></li>
                        </ul>
                    </li>








                    {{-- Профиль --}}
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{{ url('/profile') }}">@lang('menu.profile')</a></li>
                            <li class="divider"></li>
                            <li><a href="{{ url('/tickets') }}">@lang('menu.tickets')</a></li>
                            <li class="divider"></li>
                            <li>
                                <a href="{{ url('/logout') }}"
                                   onclick="event.preventDefault();
                                   document.getElementById('logout-form').submit();">
                                    @lang('auth.logout')
                                </a>

                                <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </li>


                @endif

                    {{-- Язык --}}
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                           aria-expanded="false">@lang( 'menu.'.session('locale') ) <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="/language/en">@lang('menu.en')  </a></li>
                            <li class="divider"></li>
                            <li><a href="/language/ru">@lang('menu.ru')  </a></li>
                        </ul>
                    </li>
            </ul>











        </div>
    </div>
</nav>


