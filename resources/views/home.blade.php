@extends('layout')

@section('content')
<div class="well">



    <div class="row">
        <div class="col-md-4">
            @include('parts.widget',['name'=>trans('menu.projects'), 'icon' => 'fa-sitemap '])
        </div>
        <div class="col-md-4">
            @include('parts.widget',['name'=>trans('menu.profile'), 'icon' => 'fa-info-circle '])
        </div>
        <div class="col-md-4">
            @include('parts.widget',['name'=>trans('menu.users'), 'icon' => 'fa-users'])
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            @include('parts.widget',['name'=>trans('menu.payments'), 'icon' => 'fa-credit-card-alt'])
        </div>
        <div class="col-md-4">
            @include('parts.widget',['name'=>trans('menu.statistics'), 'icon' => 'fa-line-chart'])
        </div>
        <div class="col-md-4">
            @include('parts.widget',['name'=>trans('menu.tickets'), 'icon' => 'fa-comments'])
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            @include('parts.widget',['name'=>trans('menu.files'), 'icon' => 'fa-file-text'])
        </div>
        <div class="col-md-4">
            @include('parts.widget',['name'=>trans('menu.help'), 'icon' => 'fa-question-circle-o'])
        </div>
        <div class="col-md-4">
            @include('parts.widget',['name'=>trans('menu.modules'), 'icon' => 'fa-cubes'])
        </div>
    </div>



</div>
@endsection
