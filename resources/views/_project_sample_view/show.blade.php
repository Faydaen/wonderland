@extends('layout')

@section('title') {{$project->title}} @endsection




@section('header')

@endsection




@section('content')

    <div class="well">
        <h2>{{$project->title}}</h2>

        <p>{{$project->description}}</p>

        @include('treeLayout',['projectId'=>$project->id])


    </div>




@endsection
