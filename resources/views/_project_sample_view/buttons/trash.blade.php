

@include('parts.button',[
    'method'=>'post',
    'route'=>'project.restore',
    'id'=>$project->id,
    'icon'=>'fa-reply',
])

@include('parts.button',[
    'method'=>'delete',
    'route'=>'project.destroy',
    'id'=>$project->id,
    'icon'=>'fa-trash',
])


