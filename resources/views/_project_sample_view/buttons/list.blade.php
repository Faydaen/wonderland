@include('parts.button',['method'=>'get','route'=>'project.show','id'=>$project->id,'icon'=>'fa-eye'])


{{-- Избранное --}}
@if($project->favorite == false )
    @include('parts.button',[
        'method'=>'patch',
        'route'=>'project.patch',
        'id'=>$project->id,
        'icon'=>'fa-heart-o',
        'name'=>'favorite',
        'value'=>'1',
    ])
@else
    @include('parts.button',[
        'method'=>'patch',
        'route'=>'project.patch',
        'id'=>$project->id,
        'icon'=>'fa-heart',
        'name'=>'favorite',
        'value'=>'0',
    ])
@endif


{{-- Видимое --}}
@if($project->enable == true )
    @include('parts.button',[
        'method'=>'patch',
        'route'=>'project.patch',
        'id'=>$project->id,
        'icon'=>'fa-toggle-on',
        'name'=>'enable',
        'value'=>'0',
    ])
@else
    @include('parts.button',[
        'method'=>'patch',
        'route'=>'project.patch',
        'id'=>$project->id,
        'icon'=>'fa-toggle-off',
        'name'=>'enable',
        'value'=>'1',
    ])
@endif


@include('parts.button',['method'=>'get','route'=>'project.edit','id'=>$project->id,'icon'=>'fa-pencil'])

@include('parts.button',['method'=>'delete','route'=>'project.delete','id'=>$project->id,'icon'=>'fa-trash'])
