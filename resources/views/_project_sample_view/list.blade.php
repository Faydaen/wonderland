@extends('layout')

@section('title') {{ trans('project.list') }} @endsection




@section('header')

@endsection




@section('content')
    <div class="well">
        @if($trash==true)
            <h2>{{trans('project.list.trash')}}</h2>
        @else
            <h2>{{trans('project.list')}}</h2>
        @endif
        <hr>


        @if($trash==false)
            <div class="btn-group">
                <a href="{{route('project.create')}}" class="btn btn-default">
                    <i class="fa fa-plus"></i>
                    @lang('project.create')
                </a>
                <a href="{{route('project.trash')}}" class="btn btn-default">
                    <i class="fa fa-trash"></i>
                    @lang('project.list.trash')
                </a>
            </div>
        @else
            <a href="{{route('project.list')}}" class="btn btn-default">
                <i class="fa fa-list"></i>
                @lang('project.list')
            </a>
        @endif

        <hr>


        @forelse($projects as $project)
            @include('project.item',['trash'=>$trash])
        @empty
            @if($trash==true)
                {{ trans('project.list.empty.trash') }}
            @else
                {{ trans('project.list.empty') }}
            @endif
        @endforelse


        @if($projects->total() > $projects->perPage())
            <div class="well well-sm text-center">
                <div> {{ $projects->links() }} </div>
            </div>
        @endif


    </div>
@endsection
