<?php



return [
    'en'=>'English',
    'ru'=>'Russian',
    'search' => 'Search',
    'menu'=>'Menu',


    'projects' => 'Projects',
    'profile' => 'Profile',
    'users' => 'Users',
    'payments' => 'Payments',
    'statistics' => 'Statistics',
    'tickets' => 'Tickets',
    'files' => 'Files',
    'help' => 'Help',
    'modules' => 'Modules',


    'look' => 'Look',

    
    
];