<?php

return [
    'ru'=>'Русский',
    'en'=>'Английский',

    'search'=>'Поиск',
    'find'=>'Найти',
    'edit'=>'Редактировать',
    'create'=>'Создать',
    'show'=>'Просмотр',

    'code' => 'Код',
    'code.placeholder' => 'Код',
    'save' => 'Сохранить',
    'apply' => 'Применить',
    'back' => 'Назад',
    'tags' => 'Теги',
    'choose_tags' => 'Выберете теги',

    'category' => 'Категория',
    'favorite' => 'Избранное',

    'delete'=>'Удалить',
    'restore'=>'Востановить',
    'destroy'=>'Очистить',

    'title' => 'Название',
    'title.placeholder' => 'Введите название',
    'description' => 'Описание',
    'description.placeholder' => 'Введите описание',

    'image.browse'=>'Выберете изображение',
    'image.destroy'=>'Удалить изображение',
    'image.browse.help'=>'Изображение будет изменено до размера :size пикселей',
    'file.browse'=>'Выберете файл',



];