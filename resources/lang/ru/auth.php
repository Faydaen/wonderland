<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',

    'login' =>'Войти',
    'logout' =>'Выход',

    'register' =>'Зарегистрироваться',
    'name' =>'Имя',
    'email' =>'E-Mail адрес',
    'password' =>'Пароль',
    'confirm_password' =>'Подтвертите пароль',
    'remember_me' => 'Запомнить',
    'forgot_your_password' => 'Забыли пароль?',
    'reset_password' => 'Востановить пароль',
    'send_password_reset_link' => 'Отправить ссылку для востановления паролья',


];
