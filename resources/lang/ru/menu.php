<?php



return [
    'en'=>'Английский',
    'ru'=>'Русский',
    'search' => 'Поиск',
    'menu'=>'Меню',
    
    'projects' => 'Проекты',
    'profile' => 'Профиль',
    'users' => 'Пользователи',
    'payments' => 'Платежи',
    'statistics' => 'Статискитка',
    'tickets' => 'Тикеты',
    'files' => 'Файлы',
    'help' => 'Помощь',
    'modules' => 'Модули',

    'look'=>'Смотреть',
];