<?php

return [

    'show' => 'Просмотр',
    'create' => 'Создать',
    'destroy' => 'Удалить',
    'delete' => 'Удалить',
    'edit' => 'Редактировать',

    'sync'=>'Синхронизировать',
    'directives'=>'Директивы',
    'use_apiai'=>'Использовать API.AI',
    'priority'=>'Приоритетет',

    'intent'=>'Интент',
    'flowchart'=>'Флоучарт',
    'trash'=>'Удаленные',
    'context'=>'Контекст',
    'script'=>'Скрипт',
    'test'=>'Тестировщик',
    'says'=>'Запрос',
    'says.placeholder'=>'Что сказал пользователь',
    'says.helper'=>'Каждый запрос введите с новой строки',
    'response'=>'Ответ',



    'input_context'=>'Входящий контекст',
    'input_context.placeholder'=>'Входящий контекст',
    'input_context.helper'=>'Входящий контекст',

    'output_context'=>'Исходящий контекст',
    'output_context.placeholder'=>'Исходящий контекст',
    'output_context.helper'=>'Исходящий контекст',

    'reset_context'=>'Сбросить контекст',


    'action'=>'Действие',
    'fallback'=>'Фалбек интент',
    'response.helper'=>'Ответ',
    'response.placeholder'=>'Ответ',


];