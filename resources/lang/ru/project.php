<?php

return [


    'show' => 'К проекту',
    
    
    'project'=>'Проект',
    'projects'=>'Проекты',

    'create' => 'Создать проект',
    'edit' => 'Редактировать проект',

    'apiai_client_token' => 'Ключ api.ai',
    'apiai_client_token.placeholder' => 'Ключ api.ai',
    'apiai_client_token.helper' => 'Токен сервиса <a href="https://api.ai/">api.ai</a>',


    'apiai_developer_token' => 'Ключ разработчика api.ai',
    'apiai_developer_token.placeholder' => 'Ключ разработчика api.ai',
    'apiai_developer_token.helper' => 'Для быстрого переключения между develop и production сервисами',


    'telegram_token.placeholder' => 'Ключ телеграмма',
    'telegram_token' => 'Ключ телеграмма',
    'telegram_token.helper' => 'Токен бота, который вы полчили при его создании',


    'list.trash' => 'Список удаленных проектов',
    'list.empty' => 'Нет созданный проектов',
    'list.empty.trash' => 'Корзина пуста',
    'list' => 'Список проектов',

    'dev_mode' => 'Режим разработчика',

    'logo' => 'Логотип проекта',

    'create_folder' => 'Создать папку',
    'delete_folder' => 'Удалить папку',

];