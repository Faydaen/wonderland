<?php
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App{
/**
 * App\Process
 *
 * @mixin \Eloquent
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property integer $project_id
 * @property integer $pos
 * @property string $code
 * @property boolean $is_system
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Intent[] $intents
 * @property-read \App\Project $project
 * @method static \Illuminate\Database\Query\Builder|\App\Process whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Process whereTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Process whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Process whereProjectId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Process wherePos($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Process whereCode($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Process whereIsSystem($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Process whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Process whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Process whereDeletedAt($value)
 */
	class Process extends \Eloquent {}
}

namespace App{
/**
 * App\Project
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $title
 * @property string $description
 * @property string $logo
 * @property boolean $telegram_enable
 * @property string $telegram_token
 * @property string $telegram_name
 * @property boolean $whatsapp_enable
 * @property string $whatsapp_token
 * @property string $whatsapp_name
 * @property boolean $viber_enable
 * @property string $viber_token
 * @property string $viber_name
 * @property boolean $facebook_enable
 * @property string $facebook_token
 * @property string $facebook_name
 * @property boolean $dev_mode
 * @property string $apiai_client_token
 * @property string $apiai_developer_token
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Query\Builder|\App\Project whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Project whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Project whereTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Project whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Project whereLogo($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Project whereTelegramEnable($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Project whereTelegramToken($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Project whereTelegramName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Project whereWhatsappEnable($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Project whereWhatsappToken($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Project whereWhatsappName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Project whereViberEnable($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Project whereViberToken($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Project whereViberName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Project whereFacebookEnable($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Project whereFacebookToken($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Project whereFacebookName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Project whereDevMode($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Project whereApiaiClientToken($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Project whereApiaiDeveloperToken($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Project whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Project whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Project whereDeletedAt($value)
 * @mixin \Eloquent
 * @property string $telegram
 * @property string $whatsapp
 * @property string $viber
 * @property string $facebook
 * @property string $apiai
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Process[] $process
 * @method static \Illuminate\Database\Query\Builder|\App\Project whereTelegram($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Project whereWhatsapp($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Project whereViber($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Project whereFacebook($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Project whereApiai($value)
 */
	class Project extends \Eloquent {}
}

namespace App{
/**
 * App\Profile
 *
 * @property integer $id
 * @property string $avatar
 * @property string $language
 * @property string $sex
 * @property string $phone
 * @property string $background
 * @property string $first_name
 * @property string $last_name
 * @property string $birthday
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Query\Builder|\App\Profile whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Profile whereAvatar($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Profile whereLanguage($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Profile whereSex($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Profile wherePhone($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Profile whereBackground($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Profile whereFirstName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Profile whereLastName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Profile whereBirthday($value)
 * @mixin \Eloquent
 * @property string $template
 * @method static \Illuminate\Database\Query\Builder|\App\Profile whereTemplate($value)
 */
	class Profile extends \Eloquent {}
}

namespace App{
/**
 * App\Intent
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property integer $process_id
 * @property integer $pos
 * @property string $iid
 * @property string $code
 * @property string $user_says
 * @property string $context
 * @property boolean $use_apiai
 * @property string $script
 * @property boolean $synchronized
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @method static \Illuminate\Database\Query\Builder|\App\Intent whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Intent whereTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Intent whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Intent whereProcessId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Intent wherePos($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Intent whereIid($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Intent whereCode($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Intent whereUserSays($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Intent whereContext($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Intent whereUseApiai($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Intent whereScript($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Intent whereSynchronized($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Intent whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Intent whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Intent whereDeletedAt($value)
 * @mixin \Eloquent
 * @property string $templates
 * @property string $affected_contexts
 * @property boolean $reset_contexts
 * @property string $speech
 * @property string $action
 * @property string $priority
 * @property boolean $fallback
 * @property string $sync_errors
 * @property-read \App\User $process
 * @method static \Illuminate\Database\Query\Builder|\App\Intent whereTemplates($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Intent whereAffectedContexts($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Intent whereResetContexts($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Intent whereSpeech($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Intent whereAction($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Intent wherePriority($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Intent whereFallback($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Intent whereSyncErrors($value)
 */
	class Intent extends \Eloquent {}
}

namespace App{
/**
 * App\User
 *
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property integer $profile_id
 * @property integer $client_id
 * @property string $remember_token
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $unreadNotifications
 * @method static \Illuminate\Database\Query\Builder|\App\User whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User wherePassword($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereProfileId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereClientId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereRememberToken($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Project[] $projects
 * @property-read \App\Profile $profile
 * @property-read \App\Client $client
 */
	class User extends \Eloquent {}
}

namespace App{
/**
 * App\File
 *
 * @property integer $id
 * @property string $file_type
 * @property string $file_name
 * @property string $thumbnail
 * @property boolean $uploaded_to_telegram
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @method static \Illuminate\Database\Query\Builder|\App\File whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\File whereFileType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\File whereFileName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\File whereThumbnail($value)
 * @method static \Illuminate\Database\Query\Builder|\App\File whereUploadedToTelegram($value)
 * @method static \Illuminate\Database\Query\Builder|\App\File whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\File whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\File whereDeletedAt($value)
 * @mixin \Eloquent
 * @property string $owner
 * @property integer $client_id
 * @property string $telegram_url
 * @method static \Illuminate\Database\Query\Builder|\App\File whereOwner($value)
 * @method static \Illuminate\Database\Query\Builder|\App\File whereClientId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\File whereTelegramUrl($value)
 */
	class File extends \Eloquent {}
}

namespace App{
/**
 * App\Client
 *
 * @property integer $id
 * @property integer $chat_id
 * @property string $comment
 * @property integer $project_id
 * @property string $username
 * @property string $first_name
 * @property string $last_name
 * @property string $parameters
 * @property string $tmp_parameters
 * @property string $chat_history
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @method static \Illuminate\Database\Query\Builder|\App\Client whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Client whereChatId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Client whereComment($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Client whereProjectId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Client whereUsername($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Client whereFirstName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Client whereLastName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Client whereParameters($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Client whereTmpParameters($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Client whereChatHistory($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Client whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Client whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Client whereDeletedAt($value)
 * @mixin \Eloquent
 * @property string $process_history
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Query\Builder|\App\Client whereProcessHistory($value)
 */
	class Client extends \Eloquent {}
}

