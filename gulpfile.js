const elixir = require('laravel-elixir');

elixir.config.sourcemaps = false;

elixir(function(mix) {

    var vendorPath = 'bower_components/';
    var assetsPath = 'resources/assets/';
    var publicPath = 'public/';

    // копирование
    mix.copy(
        vendorPath + 'jquery/dist/jquery.min.js',
        assetsPath + 'js/lib/jquery.min.js'
    );
    mix.copy(
        vendorPath + 'bootstrap/dist/js/bootstrap.min.js',
        assetsPath + 'js/lib/bootstrap.min.js'
    );
    mix.copy(
        vendorPath + 'bootswatch/spacelab/bootstrap.min.css',
        assetsPath + 'css/lib/bootstrap.min.css'
    );
    mix.copy(
        vendorPath + 'font-awesome/css/font-awesome.min.css',
        assetsPath + 'css/lib/font-awesome.min.css'
    );
    mix.copy(
        vendorPath + 'bootstrap-select/dist/js/bootstrap-select.min.js',
        assetsPath + 'js/lib/bootstrap-select.min.js'
    );
    mix.copy(
        vendorPath + 'bootstrap-select/dist/css/bootstrap-select.min.css',
        assetsPath + 'css/lib/bootstrap-select.min.css'
    );
    mix.copy(
        vendorPath + 'select2/dist/js/select2.full.min.js',
        assetsPath + 'js/lib/select2.full.min.js'
    );
    mix.copy(
        vendorPath + 'select2/dist/css/select2.min.css',
        assetsPath + 'css/lib/select2.min.css'
    );
    mix.copy(
        vendorPath + 'switchery/dist/switchery.min.css',
        assetsPath + 'css/lib/switchery.min.css'
    );
    mix.copy(
        vendorPath + 'switchery/dist/switchery.min.js',
        assetsPath + 'js/lib/switchery.min.js'
    );
    mix.copy(
        vendorPath + 'bootstrap-file-input/bootstrap.file-input.js',
        assetsPath + 'js/lib/bootstrap.file-input.js'
    );


    // слияние стилий
    mix.styles([
        'lib/bootstrap.min.css',
        'lib/font-awesome.min.css',
        'lib/bootstrap-select.min.css',
        'lib/select2.min.css',
        'lib/switchery.min.css'
    ], publicPath + 'compiled/main.css');

    // слияние скриптров
    mix.scripts([
        'lib/jquery.min.js',
        'lib/bootstrap.min.js',
        'lib/bootstrap-select.min.js',
        'lib/select2.full.min.js',
        'lib/switchery.min.js',
        'lib/bootstrap.file-input.js'
    ], publicPath + 'compiled/main.js');

    // шрифты
    mix.copy(
        vendorPath + 'font-awesome/fonts/',
        publicPath + '/build/fonts'
    );

    // версионность
    mix.version([
        publicPath + 'compiled/main.css',
        publicPath + 'compiled/main.js'
    ]);

});
