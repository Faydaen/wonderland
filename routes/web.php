<?php



Route::get('/', function () {
    return view('welcome');
});


Route::get('language/{lang}', ['as'=>'language','uses'=>'UserController@setLanguage']);



// Проект
Route::group(['prefix' => 'projects'],function (){
    #region просмотр
    Route::get('/','ProjectController@showList')
        ->name('project.list');

    Route::get('/{projectId}','ProjectController@show')->where(['projectId'=>'[0-9]+'])
        ->name('project.show');

    Route::get('/{projectId}/tree/','ProjectController@tree')->where(['projectId'=>'[0-9]+'])
        ->name('project.tree');
    #endregion

    #region создать
    Route::get('/create','ProjectController@showCreateForm')
        ->name('project.create');

    Route::post('/create','ProjectController@store')
        ->name('project.store');
    #endregion

    #region редаткирование
    Route::get('/{projectId}/edit','ProjectController@showUpdateForm')->where(['projectId'=>'[0-9]+'])
        ->name('project.edit');


    Route::put('/{projectId}/edit','ProjectController@update')->where(['projectId'=>'[0-9]+'])
        ->name('project.update');
    #endregion

    #region удаление
    Route::delete('/{projectId}/destroy','ProjectController@destroy')->where(['projectId'=>'[0-9]+'])
        ->name('project.destroy');

    Route::delete('/{projectId}/delete','ProjectController@moveToTrash')->where(['projectId'=>'[0-9]+'])
        ->name('project.delete');

    Route::post('/{projectId}/restore','ProjectController@restoreFormTrash')->where(['projectId'=>'[0-9]+'])
        ->name('project.restore');

    Route::get('/trash','ProjectController@showTrash')
        ->name('project.trash');
    #endregion
});











Route::group(['prefix' => 'process'],function () {

    #region просмотр
    Route::get('/','ProcessController@showList')
        ->name('process.list');

    Route::get('/{processId}','ProcessController@show')->where(['processId'=>'[0-9]+'])
        ->name('process.show');

    Route::get('/{processId}/tree/','ProcessController@tree')->where(['processId'=>'[0-9]+'])
        ->name('process.tree');
    #endregion

    #region создать
    Route::get('/create','ProcessController@showCreateForm')
        ->name('process.create');

    Route::post('/create','ProcessController@store')
        ->name('process.store');
    #endregion

    #region редаткирование
    Route::get('/{processId}/edit','ProcessController@showUpdateForm')->where(['processId'=>'[0-9]+'])
        ->name('process.edit');


    Route::put('/{processId}/edit','ProcessController@update')->where(['processId'=>'[0-9]+'])
        ->name('process.update');
    #endregion

    #region удаление
    Route::delete('/{processId}/destroy','ProcessController@destroy')->where(['processId'=>'[0-9]+'])
        ->name('process.destroy');

    Route::delete('/{processId}/delete','ProcessController@moveToTrash')->where(['processId'=>'[0-9]+'])
        ->name('process.delete');

    Route::post('/{processId}/restore','ProcessController@restoreFormTrash')->where(['processId'=>'[0-9]+'])
        ->name('process.restore');

    Route::get('/trash','ProcessController@showTrash')
        ->name('process.trash');
    #endregion

    Route::get('/{processId}/sync','ProcessController@sync')->where(['processId'=>'[0-9]+'])
        ->name('process.sync');


});




Route::group(['prefix' => 'intent'],function () {


    #region просмотр
    Route::get('/','IntentController@showList')
        ->name('intent.list');

    Route::get('/{intentId}','IntentController@show')->where(['intentId'=>'[0-9]+'])
        ->name('intent.show');
    #endregion

    #region создать
    Route::get('/create','IntentController@showCreateForm')
        ->name('intent.create');

    Route::post('/create','IntentController@store')
        ->name('intent.store');
    #endregion

    #region редаткирование
    Route::get('/{intentId}/edit','IntentController@showUpdateForm')->where(['intentId'=>'[0-9]+'])
        ->name('intent.edit');


    Route::put('/{intentId}/edit','IntentController@update')->where(['intentId'=>'[0-9]+'])
        ->name('intent.update');
    #endregion

    #region удаление
    Route::delete('/{intentId}/destroy','IntentController@destroy')->where(['intentId'=>'[0-9]+'])
        ->name('intent.destroy');

    Route::delete('/{intentId}/delete','IntentController@moveToTrash')->where(['intentId'=>'[0-9]+'])
        ->name('intent.delete');

    Route::post('/{intentId}/restore','IntentController@restoreFormTrash')->where(['intentId'=>'[0-9]+'])
        ->name('intent.restore');

    Route::get('/trash','IntentController@showTrash')
        ->name('intent.trash');
    #endregion


    Route::get('/{intentId}/sync','IntentController@sync')->where(['intentId'=>'[0-9]+'])
        ->name('intent.sync');

});





//->where(['id'=>'[0-9]+','name'=>'favorite|enable']);






Auth::routes();



Route::get('/home', 'HomeController@index');


